import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/domain/repository/___mock__/task_repository_mock.dart';
import 'package:todo_app/domain/repository/task_repository.dart';
import 'package:todo_app/domain/usecase/task_usecase.dart';

GetIt locator = GetIt.instance;

void main() {
  TaskUseCase taskUseCase;
  MockTaskRepository mockTaskRepository;

  setUp(() {
    locator.registerLazySingleton<TaskRepository>(() => MockTaskRepository());
    mockTaskRepository = locator<TaskRepository>();
    taskUseCase = TaskUseCase(taskRepository: mockTaskRepository);
  });

  tearDown(() {
    locator.reset();
  });

  group('TaskUseCase', () {
    test('should get list', () async {
      // given
      when(
        mockTaskRepository.getTasks(),
      ).thenAnswer(
        (_) async => tasks,
      );

      // when
      final response = await taskUseCase.getTasks();

      // expect
      expect(response.length, 3);
    });

    test('should generate taskId is 4', () async {
      // given
      when(
        mockTaskRepository.generateTaskId(),
      ).thenAnswer(
        (_) async => 4,
      );

      // when
      final nextTaskId = await taskUseCase.generateTaskId();

      // expect
      expect(nextTaskId, 4);
    });

    test('should generate taskId is 4', () async {
      final newTask = Task(
        taskId: 5,
        taskName: 'Count',
        isComplete: 1,
      );

      // given
      when(
        mockTaskRepository.addOrUpdateTask(newTask, true),
      ).thenAnswer(
        (_) async => null,
      );

      try {
        // when
        await taskUseCase.addOrUpdateTask(newTask, true);
      } catch (e) {
        // expect
        expect(e is Exception, false);
      }
    });
  });
}
