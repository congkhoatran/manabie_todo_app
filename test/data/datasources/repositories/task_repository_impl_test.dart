import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import 'package:todo_app/data/datasources/local/___mock__/db_task_datasource_mock.dart';
import 'package:todo_app/data/datasources/local/db_task_datasource.dart';
import 'package:todo_app/data/datasources/repositories/task_repository_impl.dart';
import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';

GetIt locator = GetIt.instance;

void main() {
  TaskRepositoryImpl taskRepositoryImpl;
  MockTaskDataSource mockTaskDataSource;

  setUp(() {
    locator.registerLazySingleton<TaskDataSource>(() => MockTaskDataSource());
    mockTaskDataSource = locator<TaskDataSource>();
    taskRepositoryImpl = TaskRepositoryImpl(taskHelper: mockTaskDataSource);
  });

  tearDown(() {
    locator.reset();
  });

  group('Task repository', () {
    test(
      'Get task list',
      () async {
        // given
        when(mockTaskDataSource.getTasks()).thenAnswer((_) async => tasks);

        // when
        final response = await taskRepositoryImpl.getTasks();

        // when
        expect(response.length, 3);
      },
    );

    test('Add new task', () async {
      final newTask = Task(
        taskId: 5,
        taskName: 'Count',
        isComplete: 1,
      );

      // given
      when(
        mockTaskDataSource.addTask(newTask),
      ).thenAnswer(
        (_) async => null,
      );

      try {
        // when
        await taskRepositoryImpl.addOrUpdateTask(newTask, true);
      } catch (e) {
        // expect
        expect(e is Exception, false);
      }
    });

    test('Update new task', () async {
      final updateTask = Task(
        taskId: 2,
        taskName: 'Count',
        isComplete: 0,
      );

      // given
      when(
        mockTaskDataSource.updateTask(updateTask),
      ).thenAnswer(
        (_) async => null,
      );

      try {
        // when
        await taskRepositoryImpl.addOrUpdateTask(updateTask, false);
      } catch (e) {
        // expect
        expect(e is Exception, false);
      }
    });

  });
}
