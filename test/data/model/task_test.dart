import 'package:test/test.dart';

import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';

void main() {
  group('TaskModel', () {
    const taskId = 0;
    const taskName = 'Sleep';
    const isComplete = 1;

    test('should set value from constructor', () {
      // When
      final taskModel = Task(
        taskId: taskId,
        taskName: taskName,
        isComplete: isComplete,
      );

      // Then
      expect(taskModel.taskId, 0);
      expect(taskModel.taskName, 'Sleep');
      expect(taskModel.isComplete, 1);
    });

    test('should set value', () {
      // When
      final taskModel = Task.fromMap(firstTaskData);

      // Then
      expect(taskModel.taskId, 0);
      expect(taskModel.taskName, 'Sleep');
      expect(taskModel.isComplete, 1);
    });

    test('FromList: should throw error for invalid type', () {
      expect(() => Task.fromMap(mockInvalidTaskData), throwsA(const TypeMatcher<dynamic>()));
    });
  });
}
