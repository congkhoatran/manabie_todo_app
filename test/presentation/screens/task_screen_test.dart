import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:mocktail/mocktail.dart';
import 'package:todo_app/presentation/blocs/task_bloc/___mock__/task_bloc_mock.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_bloc.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_state.dart';
import 'package:todo_app/presentation/screens/task_screen.dart';
import 'package:todo_app/presentation/widgets/new_task.dart';
import 'package:todo_app/presentation/widgets/task_list.dart';

GetIt locator = GetIt.instance;

void main() {
  MockTaskBloc mockTaskBloc;
  setUpAll(() {
    registerFallbackValue(
      TaskLoaded(
        tasks: [],
      ),
    );
    registerFallbackValue(GetTasks());
    registerFallbackValue(AddOrUpdateTask(task: tasks[0], addNew: true));
    mockTaskBloc = MockTaskBloc();
  });

  Widget _buildWidget() {
    return Builder(
      builder: (context) {
        return MaterialApp(
          home: Scaffold(
            body: BlocProvider<TaskBloc>(
              create: (context) => mockTaskBloc,
              child: TaskScreen(
                title: 'All',
                tasks: tasks,
                taskBlock: mockTaskBloc,
              ),
            ),
          ),
        );
      },
    );
  }

  testWidgets('Should render to do list', (WidgetTester tester) async {
    // Given
    final child = _buildWidget();

    // When
    await tester.pumpWidget(child);

    // Then
    expect(find.byType(TaskList), findsNWidgets(1));
    expect(find.byType(IconButton), findsNWidgets(5));
  });

  testWidgets('Should show add new task dialog', (WidgetTester tester) async {
    // Given
    final child = _buildWidget();

    // When
    await tester.pumpWidget(child);

    await tester.tap(find.byKey(ValueKey('AddNewTaskButton')));
    await tester.pump();

    // Then
    expect(find.byType(AlertDialog), findsNWidgets(1));
  });

  testWidgets('Should show dialog after click cancel to close',
      (WidgetTester tester) async {
    // Given
        final child = _buildWidget();

    // When
    await tester.pumpWidget(child);

    await tester.tap(find.byKey(ValueKey('AddNewTaskButton')));
    await tester.pump();

    expect(find.byType(AlertDialog), findsNWidgets(1));

    await tester.tap(find.byKey(ValueKey('btnCancel')));
    await tester.pump();

    expect(find.byType(AlertDialog), findsNWidgets(0));
  });

  testWidgets('Should show dialog after click add new task',
      (WidgetTester tester) async {
    // Given
        final child = _buildWidget();

    // When
    await tester.pumpWidget(child);

    await tester.tap(find.byKey(ValueKey('AddNewTaskButton')));
    await tester.pump();

    expect(find.byType(AlertDialog), findsNWidgets(1));

    await tester.enterText(find.byKey(ValueKey('taskTextFieldKey')), '');
    await tester.pump();

    await tester.tap(find.byKey(ValueKey('btnAddTask')));
    await tester.pump();

    expect(find.text('*Task is not empty!'), findsOneWidget);

    await tester.enterText(find.byKey(ValueKey('taskTextFieldKey')), 'Cooking');
    await tester.pump();

    await tester.tap(find.byKey(ValueKey('btnAddTask')));
    await tester.pump();

    expect(find.byType(AlertDialog), findsNWidgets(0));
  });
}
