import 'package:flutter_test/flutter_test.dart';
import '../../../lib/presentation/blocs/task_bloc/task_event.dart';

void main() {
  group('Tasks Event', () {
    test('should trigger [GetTasks]', () {
      final event = GetTasks();
      expect(event is GetTasks, true);
    });
    test('should trigger [AddOrUpdateTask]', () {
      final event = AddOrUpdateTask();
      expect(event is AddOrUpdateTask, true);
    });
  });
}
