import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/domain/usecase/___mock__/task_usecase_mock.dart';
import 'package:todo_app/domain/usecase/task_usecase.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_bloc.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_state.dart';

GetIt locator = GetIt.instance;

void main() {
  group(
    'Task bloc',
    () {
      TaskBloc taskBloc;
      MockTaskUseCase mockTaskUseCase;

      setUp(() {
        locator.registerLazySingleton<TaskUseCase>(() => MockTaskUseCase());
        mockTaskUseCase = locator<TaskUseCase>();
        taskBloc = TaskBloc(taskUseCase: mockTaskUseCase);
      });

      tearDown(() {
        taskBloc.close();
        locator.reset();
      });

      blocTest(
        'Get tasks',
        build: () => taskBloc,
        act: (bloc) async {
          when(mockTaskUseCase.getTasks()).thenAnswer((_) async => tasks);
          bloc.add(GetTasks());
        },
        expect: () => [
          isA<TaskLoaded>(),
        ],
        verify: (_) async {
          verify(mockTaskUseCase.getTasks()).called(1);
        },
      );

      blocTest(
        'Add new task',
        build: () => taskBloc,
        act: (bloc) async {
          bloc.add(AddOrUpdateTask(task: tasks[0], addNew: true));
        },
        expect: () => [
          isA<TaskLoaded>(),
        ],
      );

      blocTest(
        'Update task',
        build: () => taskBloc,
        act: (bloc) async {
          bloc.add(AddOrUpdateTask(task: tasks[0], addNew: false));
        },
        expect: () => [
          isA<TaskLoaded>(),
        ],
      );
    },
  );
}
