import 'package:flutter_test/flutter_test.dart';

import '../../../lib/presentation/blocs/task_bloc/task_state.dart';

void main() {
  group('TaskState', () {
    test('should load task state', () {
      final state = TaskLoaded(tasks: []);
      expect(state is TaskLoaded, true);
    });
  });
}
