import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/presentation/widgets/new_task.dart';

void main() {
  testWidgets('Add new task dialog', (WidgetTester tester) async {
    // Given
    final child = Builder(
      builder: (context) {
        return MaterialApp(
          home: Scaffold(
            body: NewTask(
              addTask: () {},
            ),
          ),
        );
      },
    );

    // When
    await tester.pumpWidget(child);

    // Then
    expect(find.byKey(ValueKey('btnCancel')), findsNWidgets(1));
    expect(find.byKey(ValueKey('btnAddTask')), findsNWidgets(1));
    expect(find.byType(AlertDialog), findsNWidgets(1));
  });
}
