import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/data/model/___mock__/task_mock.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/presentation/widgets/task_list.dart';

void main() {
  testWidgets('should build todo list', (WidgetTester tester) async {
    // Given
    final child = Builder(
      builder: (context) {
        return MaterialApp(
          home: Scaffold(
              body: TaskList(
                tasks: tasks,
                updateTask: () {},
              )),
        );
      },
    );

    // When
    await tester.pumpWidget(child);

    // Then
    expect(find.byType(Row), findsNWidgets(3));
  });
}
