import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/data/model/task.dart';

class TaskDataSource {
  TaskDataSource._();

  static final TaskDataSource db = TaskDataSource._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstance();
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "task.db");
    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE tblTask ("
          "taskId INTEGER PRIMARY KEY,"
          "taskName TEXT,"
          "isComplete INTEGER"
          ")");
    });
  }

  Future<List<Task>> getTasks() async {
    final db = await database;
    var response = await db.query("tblTask");
    List<Task> list = response.map((c) => Task.fromMap(c)).toList();
    return list;
  }

  Future<Task> getTaskWithId(int taskId) async {
    final db = await database;
    var response =
        await db.query("tblTask", where: "taskId = ?", whereArgs: [taskId]);
    return response.isNotEmpty ? Task.fromMap(response.first) : null;
  }

  addTask(Task task) async {
    final db = await database;
    var raw = await db.insert(
      "tblTask",
      task.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return raw;
  }

  deleteTaskWithId(int taskId) async {
    final db = await database;
    return db.delete("tblTask", where: "taskId = ?", whereArgs: [taskId]);
  }

  deleteAllTask() async {
    final db = await database;
    db.delete("tblTask");
  }

  updateTask(Task task) async {
    final db = await database;
    var response = await db.update("tblTask", task.toMap(),
        where: "taskId = ?", whereArgs: [task.taskId]);
    return response;
  }
}
