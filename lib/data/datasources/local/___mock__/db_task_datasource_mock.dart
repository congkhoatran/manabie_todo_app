import 'package:mockito/mockito.dart';
import 'package:todo_app/data/datasources/local/db_task_datasource.dart';

class MockTaskDataSource extends Mock implements TaskDataSource {}
