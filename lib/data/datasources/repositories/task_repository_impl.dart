import 'package:todo_app/data/datasources/local/db_task_datasource.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/domain/repository/task_repository.dart';
import 'dart:math';

class TaskRepositoryImpl implements TaskRepository {
  final TaskDataSource taskHelper;

  TaskRepositoryImpl({this.taskHelper});

  @override
  Future<int> generateTaskId() async {
    final tasks = await taskHelper.getTasks();
    return tasks.length > 0
        ? tasks.map((task) => task.taskId).toList().reduce(max) + 1
        : 0;
  }

  @override
  Future<List<Task>> getTasks() async {
    final tasks = await taskHelper.getTasks();
    return tasks;
  }

  @override
  Future<int> addOrUpdateTask(Task task, bool addNew) async {
    if (addNew) {
      task.taskId = await generateTaskId();
      return await taskHelper.addTask(task);
    }
    return await taskHelper.updateTask(task);
  }
}
