enum TaskTypes { ALL, COMPLETE, INCOMPLETE }

class Task {
  int taskId;
  String taskName;
  int isComplete;

  Task({
    this.taskId,
    this.taskName,
    this.isComplete,
  });

  factory Task.fromMap(Map<String, dynamic> json) => Task(
        taskId: json["taskId"],
        taskName: json["taskName"],
        isComplete: json["isComplete"],
      );

  Map<String, dynamic> toMap() => {
        "taskId": taskId,
        "taskName": taskName,
        "isComplete": isComplete,
      };
}
