import 'package:todo_app/data/model/task.dart';

final Map<String, Object> firstTaskData = {
  'taskId': 0,
  'taskName': 'Sleep',
  'isComplete': 1
};

final Map<String, Object> secondTaskData = {
  'taskId': 0,
  'taskName': 'Sleep',
  'isComplete': 1
};

final tasks = [
  Task(taskId: 0, taskName: 'Sleep', isComplete: 0),
  Task(taskId: 1, taskName: 'Swim', isComplete: 1),
  Task(taskId: 2, taskName: 'Eat', isComplete: 1),
];

const Map<String, Object> mockInvalidTaskData = null;