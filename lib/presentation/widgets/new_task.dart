import 'package:flutter/material.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/presentation/theme/theme_color.dart';

class NewTask extends StatefulWidget {
  final Function addTask;

  const NewTask({Key key, this.addTask}) : super(key: key);

  @override
  _NewTaskState createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTask> {
  TextEditingController taskController = TextEditingController();
  bool _isEmpty = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    taskController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: AppColors.bg,
      content: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextField(
              key: ValueKey('taskTextFieldKey'),
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
              ),
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.grey, fontSize: 16),
                hintText: 'Add new task',
              ),
              controller: taskController,
              //onSubmitted: (_) => submitData(),
              // onChanged: (val) {
              //   titleInput = val;
              // },
            ),
            _isEmpty
                ? Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                    ),
                    child: Text(
                      "*Task is not empty!",
                      key: ValueKey('errorMessageKey'),
                      style: TextStyle(
                        color: AppColors.red,
                        fontSize: 12,
                      ),
                    ),
                  )
                : Container(),
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    key: ValueKey('btnCancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                        color: AppColors.grey,
                      ),
                    ),
                  ),
                  FlatButton(
                    key: ValueKey('btnAddTask'),
                    onPressed: () {
                      if (taskController.text.trim().length > 0) {
                        widget.addTask(
                            Task(
                              taskName: taskController.text,
                              isComplete: 0,
                            ),
                            true);
                        Navigator.of(context).pop();
                      } else {
                        setState(() {
                          _isEmpty = true;
                        });
                      }
                    },
                    child: Text(
                      'Save',
                      style: TextStyle(
                        color: AppColors.yellow_bold_text,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
