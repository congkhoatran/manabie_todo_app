import 'package:flutter/material.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/presentation/theme/theme_color.dart';

class TaskList extends StatefulWidget {
  final List<Task> tasks;
  final Function updateTask;

  const TaskList({
    Key key,
    this.tasks,
    this.updateTask,
  }) : super(key: key);

  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  List<Widget> _buildTodoList() {
    List<Widget> todoItems = [];
    widget.tasks.forEach((task) {
      todoItems.add(
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                  icon: Icon(
                    Icons.radio_button_checked_outlined,
                  ),
                  color: task.isComplete == 1
                      ? AppColors.yellow_bold_text
                      : AppColors.grey,
                  onPressed: () {
                    task.isComplete = task.isComplete == 1 ? 0 : 1;
                    widget.updateTask(task, false);
                  }),
              Text(
                task.taskName,
                style: TextStyle(
                  color: AppColors.white70,
                  decoration:
                      task.isComplete == 1 ? TextDecoration.lineThrough : null,
                ),
              ),
            ],
          ),
        ),
      );
    });
    return todoItems;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: _buildTodoList(),
        ),
      ),
    );
  }
}
