import 'package:flutter/material.dart';

class AppColors {
  static const primaryTextColor = Color.fromRGBO(83, 253, 215, 1);
  static const pinkColor = Colors.redAccent;

  static const greenColor = Color.fromRGBO(58, 150, 121, 1);
  static const lightGreen = Color.fromRGBO(233, 251, 252, 1);
  static const grayColor = Color.fromRGBO(238, 238, 238, 1);
  static const orangeColor = Color.fromRGBO(231, 95, 95, 1);
  static const purple = Color.fromRGBO(114, 109, 237, 1);
  static const blueColor = Color.fromRGBO(63, 122, 250, 1);
  static const darkBlue = Color.fromRGBO(17, 20, 76, 1);
  static const lightGrey = Color.fromRGBO(240, 240, 240, 1);

  static const bg = Color.fromRGBO(16, 39, 52, 1);
  static const bold_bg = Color.fromRGBO(41, 64, 79, 1);
  static const yellow_bold_text = Color.fromRGBO(255, 186, 56, 1);

  static const yellow_bg = Color.fromRGBO(215, 212, 165, 1);
  static const yellow_normal_text = Color.fromRGBO(194, 194, 153, 1);

  static const lightWhite = Color.fromRGBO(221, 225, 225, 1);

  static const bgLogo = Color.fromRGBO(7, 34, 49, 1);

  static const white = Colors.white;
  static const white60 = Colors.white60;
  static const white70 = Colors.white70;
  static const black = Colors.black;
  static const blue = Colors.blue;
  static const red = Colors.red;
  static const grey = Colors.grey;
  static const greenAccent = Colors.greenAccent;
}