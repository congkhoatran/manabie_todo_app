import 'package:flutter/material.dart';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_bloc.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/theme/theme_color.dart';
import 'package:todo_app/presentation/widgets/new_task.dart';
import 'package:todo_app/presentation/widgets/task_list.dart';

class TaskScreen extends StatefulWidget {
  final List<Task> tasks;
  final TaskBloc taskBlock;
  final String title;

  const TaskScreen({
    Key key,
    this.tasks,
    this.title,
    this.taskBlock,
  }) : super(key: key);

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  void _addOrUpdateTask(Task task, bool addNew) {
    widget.taskBlock.add(AddOrUpdateTask(task: task, addNew: addNew));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bg,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.bg,
        title: Text(
          widget.title,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.add,
              color: AppColors.bg,
              size: 30,
            ),
            onPressed: () {},
          ),
          Padding(
            padding: const EdgeInsets.only(
              right: 15,
            ),
            child: IconButton(
              key: ValueKey('AddNewTaskButton'),
              icon: Icon(
                Icons.edit_outlined,
                color: AppColors.yellow_bold_text,
                size: 30,
              ),
              onPressed: () {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) {
                      return NewTask(
                        addTask: _addOrUpdateTask,
                      );
                    });
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: AppColors.bg,
          child: TaskList(
            tasks: widget.tasks,
            updateTask: _addOrUpdateTask,
          ),
        ),
      ),
    );
  }
}
