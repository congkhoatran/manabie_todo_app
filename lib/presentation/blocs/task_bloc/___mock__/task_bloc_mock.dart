import 'package:bloc_test/bloc_test.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_bloc.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_state.dart';

class MockTaskBloc extends MockBloc<TaskEvent, TaskState>
    implements TaskBloc {}

