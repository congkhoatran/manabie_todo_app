// States
import 'package:equatable/equatable.dart';
import 'package:todo_app/data/model/task.dart';

abstract class TaskState extends Equatable {
  const TaskState();

  @override
  List<Object> get props => [];
}

class TaskUninitialized extends TaskState {}

class TaskError extends TaskState {}

class TaskLoaded extends TaskState {
  final List<Task> tasks;

  const TaskLoaded({
    this.tasks,
  });

  @override
  List<Object> get props => [
        tasks,
      ];
}
