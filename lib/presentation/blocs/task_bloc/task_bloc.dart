import 'package:bloc/bloc.dart';
import 'package:todo_app/domain/usecase/task_usecase.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_state.dart';

import 'task_event.dart';
import 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  final TaskUseCase taskUseCase;

  TaskBloc({this.taskUseCase}) : super(null);

  TaskState get initialState => TaskUninitialized();

  @override
  Stream<TaskState> mapEventToState(
    TaskEvent event,
  ) async* {
    switch (event.runtimeType) {
      case GetTasks:
        yield* _mapGetTasksToState(event);
        break;

      case AddOrUpdateTask:
        yield* _mapAddOrUpdateTaskToState(event);
        break;
    }
  }

  Stream<TaskState> _mapGetTasksToState(GetTasks event) async* {
    var tasks = await taskUseCase.getTasks();

    yield TaskLoaded(
      tasks: tasks,
    );
  }

  Stream<TaskState> _mapAddOrUpdateTaskToState(AddOrUpdateTask event) async* {
    await taskUseCase.addOrUpdateTask(event.task, event.addNew);
    final tasks = await taskUseCase.getTasks();

    yield TaskLoaded(
      tasks: tasks,
    );
  }
}
