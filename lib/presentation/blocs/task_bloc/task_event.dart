import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/data/model/task.dart';

abstract class TaskEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class GetTasks extends TaskEvent {
  final TaskTypes type;
  final List<Task> tasks;

  GetTasks({
    @required this.type,
    @required this.tasks,
  });
}

class AddOrUpdateTask extends TaskEvent {
  final Task task;
  final bool addNew;

  AddOrUpdateTask({
    @required this.task,
    @required this.addNew,
  });
}
