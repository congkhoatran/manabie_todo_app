import 'package:meta/meta.dart';
import 'dart:math';
import 'package:todo_app/data/model/task.dart';
import 'package:todo_app/domain/repository/task_repository.dart';

class TaskUseCase {
  //More repository
  final TaskRepository taskRepository;

  TaskUseCase({
    @required this.taskRepository,
  });

  Future<int> generateTaskId() async {
    return taskRepository.generateTaskId();
  }

  Future<List<Task>> getTasks() async {
    return taskRepository.getTasks();
  }

  Future<int> addOrUpdateTask(Task task, bool addNew) async {
    return taskRepository.addOrUpdateTask(task, addNew);
  }
}
