import 'package:mockito/mockito.dart';
import 'package:todo_app/domain/usecase/task_usecase.dart';

class MockTaskUseCase extends Mock implements TaskUseCase {}
