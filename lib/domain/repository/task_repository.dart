import 'package:todo_app/data/model/task.dart';

abstract class TaskRepository {
  Future<int> generateTaskId();

  Future<List<Task>> getTasks();

  Future<int> addOrUpdateTask(Task task, bool addNew);
}
