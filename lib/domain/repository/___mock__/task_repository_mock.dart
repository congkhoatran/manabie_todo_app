import 'package:mockito/mockito.dart';
import 'package:todo_app/domain/repository/task_repository.dart';

class MockTaskRepository extends Mock implements TaskRepository {}
