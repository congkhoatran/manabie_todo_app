import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/data/datasources/local/db_task_datasource.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_bloc.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_event.dart';
import 'package:todo_app/presentation/blocs/task_bloc/task_state.dart';
import 'package:todo_app/presentation/screens/task_screen.dart';
import 'package:todo_app/presentation/theme/theme_color.dart';
import 'data/datasources/repositories/task_repository_impl.dart';
import 'data/model/task.dart';
import 'domain/usecase/task_usecase.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<BottomNavigationBarItem> bottomBars = [];

  List<BottomNavigationBarItem> _BuildBottomBars() {
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.list,
          size: 25,
        ),
        title: Text(
          'All Tasks',
          style: TextStyle(
            fontFamily: "OpenSans",
            fontSize: 10,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.radio_button_checked_outlined,
          size: 25,
        ),
        title: Text(
          'Complete',
          style: TextStyle(
            fontFamily: "OpenSans",
            fontSize: 10,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.radio_button_unchecked_outlined,
          size: 25,
        ),
        title: Text(
          'Incomplete',
          style: TextStyle(
            fontFamily: "OpenSans",
            fontSize: 10,
          ),
        ),
      ),
    ];
  }

  Widget buildWiget({List<Task> tasks, TaskBloc taskBloc}) {
    return CupertinoTabScaffold(
      // 4
      tabBar: CupertinoTabBar(
        activeColor: AppColors.yellow_bold_text,
        backgroundColor: AppColors.bold_bg,
        inactiveColor: Colors.grey,
        items: bottomBars,
        iconSize: 25,
      ),
      tabBuilder: (BuildContext context, int index) {
        switch (index) {
          case 0:
            return TaskScreen(
              tasks: tasks,
              title: "All task",
              taskBlock: taskBloc,
            );
            break;
          case 1:
            return TaskScreen(
              tasks: tasks.where((task) => task.isComplete == 1).toList(),
              title: "Completed tasks",
              taskBlock: taskBloc,
            );
            break;
          case 2:
            return TaskScreen(
              tasks: tasks.where((task) => task.isComplete == 0).toList(),
              title: "Incomplete tasks",
              taskBlock: taskBloc,
            );
            break;
        }
        return null;
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bottomBars = _BuildBottomBars();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => TaskBloc(
          taskUseCase: TaskUseCase(
              taskRepository: TaskRepositoryImpl(
            taskHelper: TaskDataSource.db,
          )),
        )..add(GetTasks()),
        child: BlocBuilder<TaskBloc, TaskState>(
          // ignore: missing_return
          builder: (context, state) {
            if (state == null || state is TaskUninitialized) {
              return MaterialApp(
                home: Scaffold(
                  backgroundColor: AppColors.bg,
                  body: Center(
                    child: CircularProgressIndicator(
                      backgroundColor: AppColors.yellow_bold_text,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    ),
                  ),
                ),
              );
            }
            if (state is TaskError) {
              return Center(
                child: Text('failed to fetch tasks'),
              );
            }
            if (state is TaskLoaded) {
              if (state.tasks == null) {
                return Center(
                  child: Text(
                    'No tasks available!',
                    style: TextStyle(
                      color: AppColors.yellow_bold_text,
                    ),
                  ),
                );
              }
              return buildWiget(
                tasks: state.tasks,
                taskBloc: BlocProvider.of<TaskBloc>(context),
              );
            }
          },
        ),
      ),
    );
  }
}
